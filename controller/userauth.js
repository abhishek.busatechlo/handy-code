const User = require("../model/Usermodel");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const secretKey = "WOLF";
const nodemailer = require("nodemailer");

exports.singup = async (req, res) => {
  try {
    const { name, email, password } = req.body;
    if (!password) {
      return res.status(400).json({ message: "Password is required" });
    }

    const hashedPassword = await bcrypt.hash(password, 10);

    const userExists = await User.exists({ email });
    if (userExists) {
      return res.status(400).json({ message: "User already exists" });
    }

    const user = new User({ name, email, password: hashedPassword });

    await user.save();

    res.send({ success: true, message: "User created successfully" });
  } catch (error) {
    console.error(error);
    res.status(500).send({ success: false, message: "Internal server error" });
  }
};

exports.login = async (req, res) => {
  try {
    const { email, password } = req.body;
    // Check if the user exists in the database
    const user = await User.findOne({ email });

    if (!user) {
      res.status(401).send({ message: "User Not Found" });
      return;
    }

    const isPasswordValid = await bcrypt.compare(password, user.password);
    if (!isPasswordValid) {
      return res.status(401).json({ message: "Password Not Match" });
    }
    const token = jwt.sign({ userId: user._id }, secretKey);
    res.send({ message: "Login successful", token });
  } catch (error) {
    console.error(error);
    res.status(500).send({ message: "Internal server error" });
  }
};

// unProtected Route
exports.home = async (req, res) => {
  try {
    res.send({ message: "This is home route" });
  } catch {
    res.status(500).send({ message: "Internal server error" });
  }
};

// Protected Route
exports.protected = async (req, res) => {
  try {
    res.send({ message: "This is protected Rote" });
  } catch {
    res.status(500).send({ message: "Internal server error" });
  }
};

// Forget password
exports.forgotpassword = async (req, res) => {
  const { email } = req.body;

  // Find the user in the simulated database
  const user = await User.findOne({ email });

  // If the user does not exist, return an error
  if (!user) {
    return res.status(404).json({ error: "User not found" });
  }

  // Generate a JWT token for password reset
  const token = jwt.sign({ email: user.email }, secretKey, {
    expiresIn: "15m", // Token will expire in 15 minutes
  });

  // Send the password reset link to the user's email
  const transporter = nodemailer.createTransport({
    // Configure your email service provider here
    service: "gmail",
    auth: {
      user: "Your mail",
      pass: "Your pass",
    },
  });

  const mailOptions = {
    from: "wolfivsix@gmail.com",
    to: user.email,
    subject: "Password Reset",
    text: `Click the following link to reset your password: http://localhost:5000/reset-password/${token}`,
  };

  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      return res.status(500).json({ error: "Internal server error" });
    }

    return res.status(200).json({ message: "Password reset email sent" });
  });
};

exports.resetpassword = async (req, res) => {
  const { token } = req.params;
  const { newPassword } = req.body;
  console.log(token);
  console.log(newPassword);

  try {
    // Verify the JWT token
    const decoded = jwt.verify(token, secretKey);
    console.log(decoded);

    // Find the user in the simulated database
    const user = await User.findOne({ email: decoded.email });
    console.log(user);

    // If the user does not exist, return an error
    if (!user) {
      return res.status(404).json({ error: "User not found" });
    }

    // Hash the new password
    bcrypt.hash(newPassword, 10, (err, hash) => {
      if (err) {
        return res.status(500).json({ error: "Internal server error" });
      }

      // Update the password in the simulated database
      user.password = hash;

      return res.status(200).json({ message: "Password reset successful" });
    });
  } catch (error) {
    return res.status(400).json({ error: "Invalid token" });
  }
};
